﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyReplay : MonoBehaviour {

	private const int bufferFramses = 1000;
	private MyKeyFrame[] keyFrames = new MyKeyFrame[bufferFramses];
	private Rigidbody  thisRigidbody;
	private GameManager manager;

	// Use this for initialization
	void Start() {
		manager = GameObject.FindObjectOfType<GameManager>();
		thisRigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update() {
		if (manager.isRecording) {
			Record();
		} else {
			PlayBack();
		}

	}

	void PlayBack() {
		thisRigidbody.isKinematic = true;
		int frame = Time.frameCount % bufferFramses;
		thisRigidbody.position = keyFrames[frame].pos;
		thisRigidbody.rotation = keyFrames[frame].rot;
	}

	void Record() {
		thisRigidbody.isKinematic = false;
		int frame = Time.frameCount % bufferFramses;
		float time = Time.time;
		keyFrames[frame] = new MyKeyFrame(time, thisRigidbody.position, thisRigidbody.rotation);
	}
}

public struct MyKeyFrame {

	public float time;
	public Vector3 pos;
	public Quaternion rot;

	public MyKeyFrame(float time, Vector3 pos, Quaternion rot) {
		this.time = time;
		this.pos = pos;
		this.rot = rot;
	}

}